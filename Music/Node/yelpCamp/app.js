var express = require("express")
var app = express();

var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/yelp_camp");

var BodyParser = require("body-parser");
app.use(BodyParser.urlencoded({ extended: true }));

var campgroundSchema = new mongoose.Schema(
    {
        name: String,
        image: String,
        description: String
    });

var Campground = mongoose.model("Campground", campgroundSchema)
// Campground.create(

//     {
//         name: "Mountain hill",
//         image: "https://pixabay.com/get/eb33b5082af2023ed1584d05fb1d4e97e07ee3d21cac104497f1c27ea3e4b6bf_340.jpg",
//         description:"No sunRay available here "
//     }, function (err, newlyCreated) {
//         if (err) {
//             console.log(err);
//         }
//         else {
//             console.log(newlyCreated);
//         }
//     });

app.set("view engine", "ejs");

// var campgrounds = [

//     { name: "Mountain hill", image: "https://pixabay.com/get/eb33b5082af2023ed1584d05fb1d4e97e07ee3d21cac104497f1c27ea3e4b6bf_340.jpg" },
//     { name: "Mountain hill", image: "https://pixabay.com/get/eb33b5082af2023ed1584d05fb1d4e97e07ee3d21cac104497f1c27ea3e4b6bf_340.jpg" },
//     { name: "Mountain hill", image: "https://pixabay.com/get/eb33b5082af2023ed1584d05fb1d4e97e07ee3d21cac104497f1c27ea3e4b6bf_340.jpg" },
//     { name: "Mountain hill", image: "https://pixabay.com/get/eb33b5082af2023ed1584d05fb1d4e97e07ee3d21cac104497f1c27ea3e4b6bf_340.jpg" },
//     { name: "Mountain hill", image: "https://pixabay.com/get/eb33b5082af2023ed1584d05fb1d4e97e07ee3d21cac104497f1c27ea3e4b6bf_340.jpg" },
//     { name: "Mountain hill", image: "https://pixabay.com/get/eb33b5082af2023ed1584d05fb1d4e97e07ee3d21cac104497f1c27ea3e4b6bf_340.jpg" },
//     { name: "Mountain hill", image: "https://pixabay.com/get/eb33b5082af2023ed1584d05fb1d4e97e07ee3d21cac104497f1c27ea3e4b6bf_340.jpg" },

//     // {name:"Forest",image:"https://pixabay.com/get/ec31b90f2af61c22d2524518b7444795ea76e5d004b0144394f3c57ea7eeb1_340.jpg"},

//     //  {name:"Silly" ,image:"https://pixabay.com/get/ec31b90f2af61c22d2524518b7444795ea76e5d004b0144394f3c57ea7eeb1_340.jpg"}
// ];

app.get("/", function (req, res) {
    res.render("landing");
});

app.get("/campground", function (req, res) {
    //Getting all campground from db
    Campground.find({}, function (err, getAllCampground) {
        if (err) {
            console.log(err);
        } else {
            res.render("campground", { campgrounds: getAllCampground });
        }

    });


});

app.post("/campground", function (req, res) {
    var newName = req.body.name;
    var newImage = req.body.image;
    var description = req.body.description;
    var newCampground = { name: newName, image: newImage, description: description }

    //creating new campgrround comming from form 
    Campground.create(newCampground, function (error, newAdded) {
        if (error) {
            console.log(error);
        } else {
            res.redirect("/campground");
        }
    });
});

app.get("/campground/new", function (req, res) {
    res.render("new");
});

app.get("/campgrounds/:id", function (req, res) {
    Campground.findById(req.params.id, function (err, foundCampground) {
        if (err) {
            console.log(err);
        }
        else {
            res.render("show", { campgrounds: foundCampground });
        }
    });
});

app.listen("3000", function () {
    console.log("yelpCamp server is running... ");
});